declare x=0 
cd ~/directory of fastq files

while read -r -a line; do
echo ${line[0]}
echo ${line[1]}

DIR=~/directory of fastq files

for NAME in $(find . -name ${line[0]}'_1.fastq.gz' -printf "%f\n" | sed 's/_1.fastq.gz//'); do
for ADDRESS in $(find . -name $NAME'_1.fastq.gz' | sed 's/_1.fastq.gz//'); do 

echo "$NAME"
echo "$ADDRESS" 

p1='_1.fastq.gz'
p2='_2.fastq.gz'

if [ "$x" -eq 0 ]; then 
    var1="$ADDRESS"; fi
if [ "$x" -eq 1 ]; then 
    var2="$ADDRESS"; fi

x=$((x + 1))

if [ "$x" -eq 2 ]; then

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar 
#$ -m eas

module add hisat/2.1.0
module add picard-tools
module add stringtie
module add bcftools
module add samtools

cd ~/directory of fastq files

#-- mapping reads
hisat2 -x ~/GRCh38.dna -p 8 -1' $var1$p1 '-2' $var1$p2 '-1' $var2$p1 '-2' $var2$p2 '--rg-id='${line[0]} '--rg SM:'${line[1]} '| samtools view -bS - >' $DIR$NAME.'bam

#-- sort bam file
samtools sort --threads 8' $DIR$NAME.'bam >' $DIR$NAME'_sorted.bam 

#-- remove duplicate reads
MarkDuplicates I='$DIR$NAME'_sorted.bam O='$DIR$NAME'_nodup.bam M='$DIR$NAME'_marked_dup_metrics.txt REMOVE_DUPLICATES=true

#-- remove supplementary reads
bamtools filter -tag NH:1 -in' $DIR$NAME'_nodup.bam | bamtools filter -isProperPair true -out' $DIR$NAME'_nodup_properPairs_NH.bam

#-- get summary statistics of sample quality
samtools flagstat' $NAME'_nodup_properPairs_NH.bam >' $NAME'_nodup_properPairs_NH_flagstat.txt 
stringtie -p 8 -G ~/GRCh38.gtf -o' $NAME'_nodup_properPairs_NH.gtf' $NAME'_nodup_properPairs_NH.bam' > ~/'Shell script for parallel submission'$NAME.sh

x=0
fi

done
done
done < '~/meta_data.txt'   # col1: name of file, col2: name of sample

