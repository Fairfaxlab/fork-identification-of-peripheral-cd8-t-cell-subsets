
#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M username
#$ -m eas
module add python/2.7.5
module add cellranger
cellranger mat2csv ~/outs/filtered_gene_bc_matrices_h5.h5 ~/read_count.csv
