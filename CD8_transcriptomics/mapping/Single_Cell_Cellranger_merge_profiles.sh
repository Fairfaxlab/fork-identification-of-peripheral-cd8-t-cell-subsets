 
#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M username
#$ -m eas
module add python/2.7.5
module add cellranger
cd ~/fastq files
cellranger aggr --id=APembro --csv=~/samples_names.csv --normalize=mapped

