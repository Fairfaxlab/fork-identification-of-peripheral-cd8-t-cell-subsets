
cd ~ # fastq folder

for NAME in $(find . -name '*_1.fastq.gz' -printf "%f\n" | sed 's/_1.fastq.gz//'); do
for ADDRESS in $(find . -name $NAME'_1.fastq.gz' | sed 's/_1.fastq.gz//'); do 

echo "$NAME"
echo "$ADDRESS"

p1='_1.fastq.gz'
p2='_2.fastq.gz'

DIR=~ # output folder

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M username 
#$ -m eas
module add mixcr
cd ~ # fastq folder

mixcr align -p rna-seq -s hsa -OallowPartialAlignments=true' $ADDRESS$p1 $ADDRESS$p2 $DIR$NAME'.vdjca

mixcr assemblePartial' $DIR$NAME'.vdjca' $DIR$NAME'_Rescued_it1.vdjca
mixcr assemblePartial' $DIR$NAME'_Rescued_it1.vdjca' $DIR$NAME'_Rescued_it2.vdjca
mixcr assemblePartial' $DIR$NAME'_Rescued_it2.vdjca' $DIR$NAME'_Rescued.vdjca
mixcr assemble -OaddReadsCountOnClustering=true -ObadQualityThreshold=15' $DIR$NAME'_Rescued.vdjca' $DIR$NAME'.clns

mixcr exportClones' $DIR$NAME'.clns' $DIR$NAME'.txt' > ~/$NAME.sh # save scripts per sample

qsub ~/$NAME.sh # submit the script

done
done
